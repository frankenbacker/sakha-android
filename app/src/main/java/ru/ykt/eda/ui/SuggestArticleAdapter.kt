package ru.ykt.eda.ui

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_suggest.view.*
import ru.ykt.eda.R
import ru.ykt.eda.model.Suggest

/**
 * Created by ivan on 21.03.18.
 */
class SuggestArticleAdapter: RecyclerView.Adapter<SuggestArticleAdapter.ViewHolder>() {
    private val array: MutableList<Suggest> = mutableListOf()

    fun setSuggests(articles: List<Suggest>) {
        array.clear()
        array.addAll(articles)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_suggest, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as ViewHolder).bind(array[position])
    }

    override fun getItemCount(): Int = array.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {

        }

        @SuppressLint("SetTextI18n")
        fun bind(suggest: Suggest) {
            with(itemView) {
                itemView.suggest_title.text = suggest.title
            }
        }
    }
}