package ru.ykt.eda.ui

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_article.view.*
import ru.ykt.eda.R
import ru.ykt.eda.model.Article

/**
 * Created by ivan on 20.03.18.
 */
class ArticleAdapter: RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    private val array: MutableList<Article> = mutableListOf()

    fun setArticles(articles: List<Article>) {
        array.clear()
        array.addAll(articles)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as ViewHolder).bind(array[position])
    }

    override fun getItemCount(): Int = array.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {

        }

        @SuppressLint("SetTextI18n")
        fun bind(article: Article) {
            with(itemView) {
                itemView.article_title.text = article.title
                itemView.article_text.text = Html.fromHtml(article.text)
                itemView.article_from_to.text = "${article.fromLang} → ${article.toLang}"
            }
        }
    }
}