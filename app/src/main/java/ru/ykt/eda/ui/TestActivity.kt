package ru.ykt.eda.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_test.*
import ru.ykt.eda.App
import ru.ykt.eda.R
import ru.ykt.eda.api.Api
import ru.ykt.eda.model.Article
import ru.ykt.eda.model.Suggest
import ru.ykt.eda.presentation.TestPresenter
import ru.ykt.eda.presentation.TestView
import javax.inject.Inject

/**
 * Created by ivan on 20.03.18.
 */

class TestActivity: MvpAppCompatActivity(), TestView {
    private val articleAdapter = ArticleAdapter()
    private val suggestAdapter = SuggestArticleAdapter()

    @Inject
    lateinit var api: Api

    @InjectPresenter
    lateinit var presenter: TestPresenter

    @ProvidePresenter
    fun createPresenter() = TestPresenter(api)

    override fun onCreate(savedInstanceState: Bundle?) {

        App.component.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        recView.layoutManager = LinearLayoutManager(this)
        recView.adapter = suggestAdapter

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    presenter.showArticles(query)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    presenter.showSuggests(newText)
                }
                return true
            }

        })
    }

    override fun showArticles(articles: List<Article>) {
        articleAdapter.setArticles(articles)
        recView.adapter = articleAdapter
    }

    override fun showSuggests(suggests: List<Suggest>) {
        suggestAdapter.setSuggests(suggests)
        recView.adapter = suggestAdapter
    }
}