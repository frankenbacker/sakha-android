package ru.ykt.eda.presentation

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.ykt.eda.model.Article
import ru.ykt.eda.model.Suggest

/**
 * Created by ivan on 20.03.18.
 */

@StateStrategyType(AddToEndSingleStrategy::class)
interface TestView: MvpView {
    fun showSuggests(suggests: List<Suggest>)
    fun showArticles(articles: List<Article>)
}