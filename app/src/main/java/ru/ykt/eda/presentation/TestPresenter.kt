package ru.ykt.eda.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.ykt.eda.api.Api
import ru.ykt.eda.model.QueryData
import ru.ykt.eda.model.Suggest

/**
 * Created by ivan on 20.03.18.
 */

@InjectViewState
class TestPresenter constructor(private val api: Api): MvpPresenter<TestView>() {

    fun showArticles(text: String) {
        getArticles(text)
    }

    fun showSuggests(text: String) {
        getSuggests(text)
    }

    private fun getArticles(text: String) {
        api.getArticles(text).enqueue(object : Callback<QueryData> {
            override fun onResponse(call: Call<QueryData>?, response: Response<QueryData>?) {
                if (response != null) {
                    if (response.body()!!.articles.isNotEmpty()) {
                        viewState.showArticles(response.body()!!.articles[0].articles)
                    }
                }
            }

            override fun onFailure(call: Call<QueryData>?, t: Throwable?) {
            }
        })
    }

    private fun getSuggests(text: String) {
        api.getSuggestArticles(text).enqueue(object : Callback<List<Suggest>> {
            override fun onResponse(call: Call<List<Suggest>>?, response: Response<List<Suggest>>?) {
                if (response?.body() != null) {
                    viewState.showSuggests(response.body()!!)
                }
            }

            override fun onFailure(call: Call<List<Suggest>>?, t: Throwable?) {
            }
        })
    }
}