package ru.ykt.eda.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.ykt.eda.api.Api
import javax.inject.Singleton

/**
 * Created by ivan on 23.03.18.
 */

@Module
class NetModule constructor(private var baseUrl: String) {
    @Provides
    @Singleton
    fun provideApi(): Api {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api::class.java)
    }
}