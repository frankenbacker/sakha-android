package ru.ykt.eda.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ivan on 23.03.18.
 */

@Module
class AppModule constructor(private var app: Application) {
    @Provides
    @Singleton
    fun provideApp(): Context {
        return app
    }
}