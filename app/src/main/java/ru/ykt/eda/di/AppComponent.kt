package ru.ykt.eda.di

import dagger.Component
import ru.ykt.eda.ui.TestActivity
import javax.inject.Singleton

/**
 * Created by ivan on 23.03.18.
 */

@Singleton
@Component(modules = arrayOf(AppModule::class, NetModule::class))
interface AppComponent {
    fun inject(testActivity: TestActivity)
}