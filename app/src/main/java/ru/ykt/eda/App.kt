package ru.ykt.eda

import android.app.Application
import ru.ykt.eda.di.AppComponent
import ru.ykt.eda.di.AppModule
import ru.ykt.eda.di.DaggerAppComponent
import ru.ykt.eda.di.NetModule

/**
 * Created by ivan on 20.03.18.
 */

class App: Application() {
    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        component = buildComponent()
    }

    fun buildComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .netModule(NetModule("http://sakhatyla.ru/"))
                .build()
    }
}