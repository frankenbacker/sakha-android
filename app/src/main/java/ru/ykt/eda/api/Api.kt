package ru.ykt.eda.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.ykt.eda.model.QueryData
import ru.ykt.eda.model.Suggest

/**
 * Created by ivan on 20.03.18.
 */

interface Api {
    @GET("api//articles/translate")
    fun getArticles(@Query("query") word: String): Call<QueryData>

    @GET("api//articles/suggest")
    fun getSuggestArticles(@Query("query") word: String): Call<List<Suggest>>
}