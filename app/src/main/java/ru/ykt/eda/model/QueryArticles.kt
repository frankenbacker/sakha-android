package ru.ykt.eda.model

import com.google.gson.annotations.SerializedName

/**
 * Created by ivan on 21.03.18.
 */

data class QueryArticles(
        @SerializedName("FromLanguageName") val fromLang: String,
        @SerializedName("ToLanguageName") val toLang: String,
        @SerializedName("Articles") val articles: List<Article>
)