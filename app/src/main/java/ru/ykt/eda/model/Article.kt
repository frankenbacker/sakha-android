package ru.ykt.eda.model

import com.google.gson.annotations.SerializedName

/**
 * Created by ivan on 21.03.18.
 */

data class Article(
        @SerializedName("Id") val id: Int,
        @SerializedName("Title") val title: String,
        @SerializedName("Text") val text: String,
        @SerializedName("FromLanguageName") val fromLang: String,
        @SerializedName("ToLanguageName") val toLang: String,
        @SerializedName("CategoryName") val catName: String
)