package ru.ykt.eda.model

import com.google.gson.annotations.SerializedName

/**
 * Created by ivan on 22.03.18.
 */

data class Suggest(
        @SerializedName("Id") val id: Int,
        @SerializedName("Title") val title: String
)