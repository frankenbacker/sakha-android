package ru.ykt.eda.model

import com.google.gson.annotations.SerializedName

/**
 * Created by ivan on 20.03.18.
 */
data class QueryData(
        @SerializedName("Query") val query: String,
        @SerializedName("Alternatives") val alts: String,
        @SerializedName("Articles") val articles: List<QueryArticles>,
        @SerializedName("MoreArticles") val more: List<Article>
)